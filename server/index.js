import express from 'express';
import cors from 'cors';
import apiController from './controllers/apiController';

const app = express();
const port = process.env.PORT || 3000;

app.use(cors());
app.get('/api/v1/products/:id', apiController.getProduct);
app.get('/api/v1/categories/:id/products', apiController.getCategoryProducts);
app.get('/api/v1/categories/:id', apiController.getCategory);
app.get('/api/v1/categories/', apiController.getCategories);

app.listen(port, () => {
    console.log(`Application is listening on port ${port}`);
});
